# Update

Currently I am rethinking the way I want to use my shared stuff. Therefore any following
instructions are prone to errors.

# Installation

Disclaimer: I am not responsible if you brake your system trying to use this repo.

Clone this repo into the directory of your choice and symlink it to your TDS `texmf/local`
subdirectory (execute `ln -s /path/to/latex-preamble latex-preamble` inside that directory).

You may run `kpsewhich -var-value=TEXMFHOME` to find your personal `texmf` directory (so that only
you are able to include these files), or choose `/usr/local/texlive/texmf-local/tex/local/`
(assuming that you have MacTeX or Tex Live installed; so that every user on your system should be
able to include these files). Technically `/.../texmf/local/` should have worked, too, but on my
system it did not. So choosing the former you are on the safe side.

Don't forget to run `texhash` (inside whatever directory) whenever there is a new file added to this
repo.

# Include

Since `texhash` searches recursively for files that are includable, you don't have to specify any
folder. However, if there is a "naming conflict" (a file in the same folder as your main `.tex` file
or the like) the local file will be used. That's why I include them in the preamble as follows:
```
\input{share/commands}
\input{share/packages}
```

# References

* [A Directory Structure for TeX Files (TDS)](ftp://ftp.fu-berlin.de/tex/CTAN/tds/tds.pdf)
* [Where do I place my own .sty or .cls files, to make them available to all my .tex files?](https://tex.stackexchange.com/questions/1137/where-do-i-place-my-own-sty-or-cls-files-to-make-them-available-to-all-my-te)
* [Setting TEXINPUTS in TeXstudio Mac OS X](https://tex.stackexchange.com/questions/223284/setting-texinputs-in-texstudio-mac-os-x#250342)
* [\input and absolute paths](https://tex.stackexchange.com/questions/21904/input-and-absolute-paths#24815)

